# Installation


## For Development

### Configuration
<p>To prevent exposing some variables like secret keys from the public you should use environment variables.<br>
An "environment variable" is a value stored in the system memory of the device running your app. Environment variables can be temporarily created via the terminal like so:</p>

#### Windows:
```shell
$ set SECRET_KEY='yoursecretkey'
```

#### Linux:
```shell
$ export SECRET_KEY='yoursecretkey'
```

The environment variable will exist as long as you keep the terminal open.
We can keep the variables by setting them in a local file called `.env` .

#### Development Configuration
```.env
FLASK_ENV="development"
SECRET_KEY="secretkey"
```

#### Production Configuration
```.env
FLASK_ENV="production"
SECRET_KEY="secretkey"
```

#### Database Path
The path of the database can also be saved as an environment variable for security reasons. Note that a complete absolute path must be given.
Otherwise it is stored in the config file.

`.env File`: (windows path)
```.env
SQLALCHEMY_DATABASE_URI = 'C:\xxx\xxx\xxx\Carsharing_New\application\database\db_dev.sqlite3'
```

`config.py`:
```python
SQLALCHEMY_DATABASE_URI = environ.get('SQLALCHEMY_DATABASE_URI')
```

### Windows
```shell
$ git clone https://gitlab.com/it-girls/carsharing_backend
$ cd carsharing_backend
$ python -m venv env
$ .\env\Scripts\activate
$ pip install -r requirements.txt
$ python wsgi.py
```

### Linux
```shell
$ git clone https://gitlab.com/it-girls/carsharing_backend
$ cd carsharing_backend
$ python3 -m venv env
$ source env/bin/activate
$ pip3 install -r requirements.txt
$ python3 wsgi.py
```

## For Production

For installation instructions of the production system visit the [repository of the full project](https://gitlab.com/it-girls/carsharing "IT-Girls - Carsharing").