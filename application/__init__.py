""" Application base file """

__author__ = "Markus Mogck"
__version__ = "1.0"

from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from application.resources.errors import errors


db = SQLAlchemy()
api = Api()


def create_app(config):
	# construct the core application
	app = Flask(__name__, instance_relative_config=False)

	app.config.from_object(config)

	with app.app_context():
		# connects the database and api with the app
		db.init_app(app)
		api = Api(app, errors=errors) #init_app() does not work !!
		
		# import routes and initialize the database tables
		import application.database.models
		from application.resources.routes import initialize_routes
		initialize_routes(api)
		db.create_all()

		# initialize swaggerui api documentation
		from application.resources.routes import initialize_swagger
		initialize_swagger(app)

		# initialize static database values and admin account (if not existing)
		from application.database.db_init_values import initialize_values, initialize_admin
		initialize_values()
		initialize_admin()

		return app