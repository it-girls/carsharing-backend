""" Provides database folder base path """

__author__ = "Markus Mogck"
__version__ = "1.0"

from os import path
db_base_path = path.abspath(path.dirname(__file__))