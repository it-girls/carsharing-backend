""" Initialization of database entries with production defaults """

__author__ = "Markus Mogck"
__version__ = "1.0"

import uuid
from contextlib import suppress

from werkzeug.security import generate_password_hash

from application.database.models import (BookingStatus, User, UserRole,
                                         VehicleAddition, VehicleCategorie,
                                         VehicleTransmission, VehicleType, db)



def initialize_values():
	db.session.rollback()
	
	# Initialize Roles
	benutzer        = UserRole(power=1, role='benutzer')
	mitarbeiter     = UserRole(power=3, role='mitarbeiter')
	admin           = UserRole(power=6, role='admin')

	with suppress(Exception): db.session.add(benutzer)
	with suppress(Exception): db.session.add(mitarbeiter)
	with suppress(Exception): db.session.add(admin)

	# Initialize Booking states

	bestellt        = BookingStatus(status='bestellt')
	bezahlt         = BookingStatus(status='bezahlt')
	aktiv           = BookingStatus(status='aktiv')
	abgeschlossen   = BookingStatus(status='abgeschlossen')
	storniert       = BookingStatus(status='storniert')

	with suppress(Exception): db.session.add(bestellt)
	with suppress(Exception): db.session.add(bezahlt)
	with suppress(Exception): db.session.add(aktiv)
	with suppress(Exception): db.session.add(abgeschlossen)
	with suppress(Exception): db.session.add(storniert)

	# Initialize Categories
	vc              = [None] * 19
	vc[1]           = VehicleCategorie(flag='M', description='Mini (Kleinstwagen)')
	vc[2]           = VehicleCategorie(flag='N', description='Mini Elite')
	vc[3]           = VehicleCategorie(flag='E', description='Economy (Kleinwagen)')
	vc[4]           = VehicleCategorie(flag='H', description='Economy Elite')
	vc[5]           = VehicleCategorie(flag='C', description='Compact (Kompaktklasse)')
	vc[6]           = VehicleCategorie(flag='D', description='Compact Elite')
	vc[7]           = VehicleCategorie(flag='I', description='Intermediate (Mittelklasse)')
	vc[8]           = VehicleCategorie(flag='J', description='Intermediate Elite')
	vc[9]           = VehicleCategorie(flag='S', description='Standard (Mittelklasse)')
	vc[10]          = VehicleCategorie(flag='R', description='Standard Elite')
	vc[11]          = VehicleCategorie(flag='F', description='Fullsize (Mittelklasse)')
	vc[12]          = VehicleCategorie(flag='G', description='Fullsize Elite')
	vc[13]          = VehicleCategorie(flag='P', description='Premium (Obere Mittelklasse)')
	vc[14]          = VehicleCategorie(flag='U', description='Premium Elite')
	vc[15]          = VehicleCategorie(flag='L', description='Luxury (Obere Mittelklasse)')
	vc[16]          = VehicleCategorie(flag='W', description='Luxury Elite')
	vc[17]          = VehicleCategorie(flag='O', description='Oversize')
	vc[18]          = VehicleCategorie(flag='X', description='Special (Oberklasse)')

	for i in range(1,19):
	   with suppress(Exception): db.session.add(vc[i])

	# Initialize Types
	vt              = [None] * 23
	vt[1]           = VehicleType(flag='B', description='2-3 Door (Zwei- bis Dreitürer)')
	vt[2]           = VehicleType(flag='C', description='2/4 Door (Zwei- oder Viertürer)')
	vt[3]           = VehicleType(flag='D', description='4-5 Door Car (Vier- bis Fünftürer)')
	vt[4]           = VehicleType(flag='W', description='Wagon/Estate (Kombi)')
	vt[5]           = VehicleType(flag='V', description='Passenger Van')
	vt[6]           = VehicleType(flag='L', description='Limousine')
	vt[7]           = VehicleType(flag='S', description='Sport (Sportwagen)')
	vt[8]           = VehicleType(flag='T', description='Convertible (Cabriolet)')
	vt[9]           = VehicleType(flag='F', description='SUV (Sport Utility Vehicle)')
	vt[10]          = VehicleType(flag='J', description='Open Air All Terrain')
	vt[11]          = VehicleType(flag='X', description='Special (bspw. Navigationssystem)')
	vt[12]          = VehicleType(flag='P', description='Pickup Regular Cab')
	vt[13]          = VehicleType(flag='Q', description='Pickup Extended Cab')
	vt[14]          = VehicleType(flag='Z', description='Special Offer Car')
	vt[15]          = VehicleType(flag='E', description='Coupe')
	vt[16]          = VehicleType(flag='M', description='Monospace')
	vt[17]          = VehicleType(flag='R', description='Recreational Vehicle')
	vt[18]          = VehicleType(flag='H', description='Motor Home (Wohnmobil)')
	vt[19]          = VehicleType(flag='Y', description='2 Wheel Vehicle')
	vt[20]          = VehicleType(flag='N', description='Roadster')
	vt[21]          = VehicleType(flag='G', description='Crossover')
	vt[22]          = VehicleType(flag='K', description='Commercial Van/Truck (Transporter)')

	for i in range(1,23):
		with suppress(Exception): db.session.add(vt[i])

	# Initialize Transmissions
	vtr             = [None] * 7
	vtr[1]          = VehicleTransmission(flag='M', description='Manual Unspecified Drive')
	vtr[2]          = VehicleTransmission(flag='N', description='Manual 4WD')
	vtr[3]          = VehicleTransmission(flag='C', description='Manual AWD')
	vtr[4]          = VehicleTransmission(flag='A', description='Auto Unspecified')
	vtr[5]          = VehicleTransmission(flag='B', description='Auto 4WD')
	vtr[6]          = VehicleTransmission(flag='D', description='Auto AWD')

	for i in range(1,7):
		with suppress(Exception): db.session.add(vtr[i])

	# Initialize Additions
	va              = [None] * 19
	va[1]           = VehicleAddition(flag='R', description='Unspecified Fuel/Power With AC')
	va[2]           = VehicleAddition(flag='N', description='Unspecified Fuel/Power Without AC')
	va[3]           = VehicleAddition(flag='D', description='Diesel AC')
	va[4]           = VehicleAddition(flag='Q', description='Diesel No AC')
	va[5]           = VehicleAddition(flag='H', description='Hybrid AC')
	va[6]           = VehicleAddition(flag='I', description='Hybrid No AC')
	va[7]           = VehicleAddition(flag='E', description='Electric AC')
	va[8]           = VehicleAddition(flag='C', description='Electric No AC')
	va[9]           = VehicleAddition(flag='L', description='LPG/Compressed Gas AC')
	va[10]          = VehicleAddition(flag='S', description='LPG/Compressed Gas No AC')
	va[11]          = VehicleAddition(flag='A', description='Hydrogen AC')
	va[12]          = VehicleAddition(flag='B', description='Hydrogen No AC')
	va[13]          = VehicleAddition(flag='M', description='Multi Fuel/Power AC')
	va[14]          = VehicleAddition(flag='F', description='Multi fuel/power No AC')
	va[15]          = VehicleAddition(flag='V', description='Petrol AC')
	va[16]          = VehicleAddition(flag='Z', description='Petrol No AC')
	va[17]          = VehicleAddition(flag='U', description='Ethanol AC')
	va[18]          = VehicleAddition(flag='X', description='Ethanol No AC')

	for i in range(1,19):
		with suppress(Exception): db.session.add(va[i])


	# Commit to Session
	with suppress(Exception): db.session.commit()

# Initialize Admin user if not existing
# NOTE: Password should be changed immediately!
def initialize_admin():
	db.session.rollback()

	users = User.query.all()
	admin_existing = False

	for user in users:
		if user.user_role_br.role == 'admin':
			admin_existing = True

	if not admin_existing:
		admin = User(
			public_user_id=str(uuid.uuid4()),
			email='admin',
			password=generate_password_hash('admin', method='sha256'),
			user_role_id=3,
			first_name='Admin',
			last_name='Admin',
			birthdate='',
			id_number='',
			license_number=''
		)

		with suppress(Exception): db.session.add(admin)

	# Commit to Session
	with suppress(Exception): db.session.commit()
