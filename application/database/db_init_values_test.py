""" Initialization of database entries for testing purposes """

__author__ = "Markus Mogck"
__version__ = "1.0"

import datetime
from application.database.models import db, User, Station, Vehicle, Booking

def obj_exists(cls_obj, filter_by, filter):
	filters = {str(filter_by) : str(filter)}
	return db.session.query(
    	cls_obj.query.filter_by(**filters).exists()
	).scalar()
	
def initialize_test_values():
	from contextlib import suppress

	# UUID are replaced with given unique strings
	# because of that it can be tested better and it makes no difference to generated uuids

	# Add Test Users
	import uuid
	from werkzeug.security import generate_password_hash
	user1 = User(
		public_user_id='user1_uuid',
		email='user1',
		password=generate_password_hash('user1', method='sha256'),
		user_role_id=1,
		first_name='User',
		last_name='One',
		birthdate=datetime.datetime(1970, 6, 12),
		id_number='T22000129',
		license_number='B072RRE2I55'
	)
	user2 = User(
		public_user_id='user2_uuid',
		email='user2',
		password=generate_password_hash('user2', method='sha256'),
		user_role_id=1,
		first_name='User',
		last_name='Two',
		birthdate=datetime.datetime(1986, 1, 10),
		id_number='T24920166',
		license_number='B072RR7HJ6'
	)
	employee1 = User(
		public_user_id='employee1_uuid',
		email='employee1',
		password=generate_password_hash('employee1', method='sha256'),
		user_role_id=2,
		first_name='Employee',
		last_name='One',
		birthdate=datetime.datetime(1980, 4, 2),
		id_number='',
		license_number=''
	)
	employee2 = User(
		public_user_id='employee2_uuid',
		email='employee2',
		password=generate_password_hash('employee2', method='sha256'),
		user_role_id=2,
		first_name='Employee',
		last_name='Two',
		birthdate=datetime.datetime(1997, 2, 12),
		id_number='',
		license_number=''
	)
	admin = User(
		public_user_id='admin_uuid',
		email='admin',
		password=generate_password_hash('admin', method='sha256'),
		user_role_id=3,
		first_name='Admin',
		last_name='Admin',
		birthdate='',
		id_number='',
		license_number=''
	)

	
	if not obj_exists(User, 'email', user1.email): db.session.add(user1)
	if not obj_exists(User, 'email', user2.email): db.session.add(user2)
	if not obj_exists(User, 'email', employee1.email): db.session.add(employee1)
	if not obj_exists(User, 'email', employee2.email): db.session.add(employee2)
	if not obj_exists(User, 'email', admin.email): db.session.add(admin)

	# Add Test Stations
	s              = [None] * 5
	s[1]           = Station(public_station_id='s1_uuid',name='S1',address='Breitenweg 69',post_code='28195',city='Bremen',country='Deutschland')
	s[2]           = Station(public_station_id='s2_uuid',name='S2',address='Schloßgartenstraße 14',post_code='06406',city='Bernburg',country='Deutschland')
	s[3]           = Station(public_station_id='s3_uuid',name='S3',address='Peters Ave 1',post_code='00000',city='Midway',country='USA')
	s[4]           = Station(public_station_id='s4_uuid',name='S4',address='add4',post_code='44444',city='c4',country='cntr4')

	for i in range(1,5):
		if not obj_exists(Station, 'name', s[i].name): db.session.add(s[i])


	# Add Test Vehicles
	v              = [None] * 9
	v[1]           = Vehicle(public_vehicle_id='v1_uuid', chassis_number='C1',license_plate='L1', station_id=1, comment='comment1', brand='B1', model='M1', category='M', vehicle_type='B', transmission='M', addition='R')
	v[2]           = Vehicle(public_vehicle_id='v2_uuid', chassis_number='C2',license_plate='L2', station_id=1, comment='', brand='B2', model='M2', category='N', vehicle_type='C', transmission='N', addition='N')
	v[3]           = Vehicle(public_vehicle_id='v3_uuid', chassis_number='C3',license_plate='L3', station_id=1, comment='comment2', brand='B3', model='M3', category='E', vehicle_type='D', transmission='C', addition='D')
	v[4]           = Vehicle(public_vehicle_id='v4_uuid', chassis_number='C4',license_plate='L4', station_id=2, comment='', brand='B4', model='M4', category='H', vehicle_type='W', transmission='A', addition='Q')
	v[5]           = Vehicle(public_vehicle_id='v5_uuid', chassis_number='C5',license_plate='L5', station_id=2, comment='comment3', brand='B5', model='M5', category='C', vehicle_type='V', transmission='B', addition='C')
	v[6]           = Vehicle(public_vehicle_id='v6_uuid', chassis_number='C6',license_plate='L6', station_id=2, comment='', brand='B6', model='M6', category='D', vehicle_type='L', transmission='D', addition='A')
	v[7]           = Vehicle(public_vehicle_id='v7_uuid', chassis_number='C7',license_plate='L7', station_id=3, comment='comment4', brand='B7', model='M7', category='I', vehicle_type='S', transmission='M', addition='B')
	v[8]           = Vehicle(public_vehicle_id='v8_uuid', chassis_number='C8',license_plate='L8', station_id=3, comment='', brand='B8', model='M8', category='J', vehicle_type='T', transmission='N', addition='H')

	for i in range(1,9):
	   if not obj_exists(Vehicle, 'chassis_number', v[i].chassis_number): db.session.add(v[i])


	# Add Test Bookings
	b              = [None] * 4
	b[1]           = Booking(
                public_booking_id='b1_uuid',
                user_id=2,
                vehicle_id=1,
                start_date=datetime.datetime(2020, 6, 20),
                end_date=datetime.datetime(2020, 6, 22),
                status_id=3,
                fee=40,
                comment="")
	b[2]           = Booking(
                public_booking_id='b2_uuid',
                user_id=2,
                vehicle_id=4,
                start_date=datetime.datetime(2020, 6, 10),
                end_date=datetime.datetime(2020, 6, 16),
                status_id=5,
                fee=120,
                comment="")
	b[3]           = Booking(
                public_booking_id='b3_uuid',
                user_id=3,
                vehicle_id=3,
                start_date=datetime.datetime(2020, 6, 24),
                end_date=datetime.datetime(2020, 7, 3),
                status_id=1,
                fee=200,
                comment="")

	for i in range(1,4):
		with suppress(Exception): db.session.add(b[i])


	# Commit to Session
	with suppress(Exception): db.session.commit()