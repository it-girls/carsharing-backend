""" Contains all database models """

__author__ = "Markus Mogck"
__version__ = "1.0"

from application import db
from datetime import datetime

class Test(db.Model):
	__tablename__       = 'test'

	id                  = db.Column(db.String(1), primary_key=True)
	text				= db.Column(db.String(64))


class User(db.Model):
	__tablename__       = 'user'

	id                  = db.Column(db.Integer, primary_key=True)
	public_user_id      = db.Column(db.String(64), unique=True, nullable=False)
	
	email               = db.Column(db.String(64), unique=True, nullable=False)
	password            = db.Column(db.String(64), nullable=False)
	user_role_id        = db.Column(db.Integer, db.ForeignKey('user_role.id'))
	creation_date       = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

	first_name          = db.Column(db.String(64))
	last_name           = db.Column(db.String(64))
	birthdate           = db.Column(db.Integer)
	id_number           = db.Column(db.Integer)
	license_number      = db.Column(db.Integer)

	preference          = db.relationship('UserPreference', backref='user_br')
	booking             = db.relationship('Booking', backref='user_br')


class UserRole(db.Model):
	__tablename__       = 'user_role'

	id                  = db.Column(db.Integer, primary_key=True, nullable=False)
	power               = db.Column(db.Integer, unique=True, nullable=False)
	role                = db.Column(db.String(64), unique=True, nullable=False)

	user                = db.relationship('User', backref='user_role_br')


class UserPreference(db.Model):
	__tablename__       = 'user_preference'

	id                  = db.Column(db.Integer, primary_key=True, nullable=False)
	user_id             = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)


class Booking(db.Model):
	__tablename__       = 'booking'

	id                  = db.Column(db.Integer, primary_key=True)
	public_booking_id   = db.Column(db.String(64), unique=True, nullable=False)
	
	user_id             = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
	vehicle_id          = db.Column(db.Integer, db.ForeignKey('vehicle.id'), nullable=False)
	start_date          = db.Column(db.DateTime, nullable=False)
	end_date            = db.Column(db.DateTime, nullable=False)
	booking_date        = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
	status_id           = db.Column(db.Integer, db.ForeignKey('booking_status.id'))
	fee					= db.Column(db.Integer, nullable=False)
	comment             = db.Column(db.String(64))


class BookingStatus(db.Model):
	__tablename__ = 'booking_status'

	id                  = db.Column(db.Integer, primary_key=True)
	status              = db.Column(db.String(64))

	booking             = db.relationship('Booking', backref='status_br')


class Station(db.Model):
	__tablename__       = 'station'

	id                  = db.Column(db.Integer, primary_key=True)
	public_station_id   = db.Column(db.String(64), unique=True, nullable=False)

	name                = db.Column(db.String(64), unique=True)
	address             = db.Column(db.String(64))
	post_code           = db.Column(db.String(64))
	city                = db.Column(db.String(64))
	country             = db.Column(db.String(64))

	vehicle             = db.relationship('Vehicle', backref='station_br')


class Vehicle(db.Model):
	__tablename__       = 'vehicle'

	id                  = db.Column(db.Integer, primary_key=True, nullable=False)
	public_vehicle_id   = db.Column(db.String(64), unique=True, nullable=False)
	chassis_number  	= db.Column(db.String(64), unique=True)

	license_plate   	= db.Column(db.String(64))
	station_id      	= db.Column(db.Integer, db.ForeignKey('station.id'), nullable=False)
	comment         	= db.Column(db.String(64))

	brand           	= db.Column(db.String(64))
	model           	= db.Column(db.String(64))
	category        	= db.Column(db.String(1), db.ForeignKey('vehicle_categorie.flag'))
	vehicle_type    	= db.Column(db.String(1), db.ForeignKey('vehicle_type.flag'))
	transmission    	= db.Column(db.String(1), db.ForeignKey('vehicle_transmission.flag'))
	addition        	= db.Column(db.String(1), db.ForeignKey('vehicle_addition.flag'))

	booking             = db.relationship('Booking', backref='vehicle_br')

class VehicleCategorie(db.Model):
	__tablename__       = 'vehicle_categorie'

	flag                = db.Column(db.String(1), primary_key=True, nullable=False)
	description         = db.Column(db.String(64))

	backref             = db.relationship('Vehicle', backref='category_br')


class VehicleType(db.Model):
	__tablename__       = 'vehicle_type'

	flag                = db.Column(db.String(1), primary_key=True, nullable=False)
	description         = db.Column(db.String(64))

	backref             = db.relationship('Vehicle', backref='vehicle_type_br')


class VehicleTransmission(db.Model):
	__tablename__       = 'vehicle_transmission'

	flag                = db.Column(db.String(1), primary_key=True, nullable=False)
	description         = db.Column(db.String(64))

	backref             = db.relationship('Vehicle', backref='transmission_br')


class VehicleAddition(db.Model):
	__tablename__       = 'vehicle_addition'

	flag                = db.Column(db.String(1), primary_key=True, nullable=False)
	description         = db.Column(db.String(64))

	backref             = db.relationship('Vehicle', backref='addition_br')

