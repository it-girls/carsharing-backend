""" Login path and authorization decoraters """

__author__ = "Markus Mogck"
__version__ = "1.0"

import datetime
import inspect
from functools import wraps

import jwt
from flask import current_app as app
from flask import jsonify, request
from flask_restful import Resource
from werkzeug.security import check_password_hash, generate_password_hash

from application.database.models import db
from application.resources.errors import *


# Adds current user to keyword arguments
def get_current_user(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		token = None

		# 'x-access-token' must be a key in the request headers
		if 'x-access-token' in request.headers:
			token = request.headers['x-access-token']

		if not token:
			# Error 'specifically for use when authentication is required and has failed or has not yet been provided' ~wiki
			raise UnauthorizedError

		try: 
			# Decodes the token, verifies the signature (data is only base64 decoded)
			data = jwt.decode(token, app.config['SECRET_KEY'], algorithm='HS256')
		except:
			# For example, if the signature expired or incorrect data was sent
			raise BadRequestError
		
		# Search for user in the database (by public user id)
		from application.database.models import User
		current_user = User.query.filter_by(public_user_id=data['public_user_id']).first()

		# Error if the user does not exist
		if not current_user:
			raise NotFoundError
		
		# Return the current user as a keyword argument in the decorated function
		kwargs['current_user'] = current_user
		
		return f(*args, **kwargs)

	return decorated

# Used when a request tries to add a record with an id that differs from the id of the person submitting it
def get_foreign_add_power(required_power):
	def wrapped(f):
		@wraps(f)
		@get_current_user
		def decorated(*args, **kwargs):
			# Add 'foreign_add_power' to keyword arguments if needed by decorated function
			if 'foreign_add_power' in inspect.getfullargspec(f).args:
				kwargs['foreign_add_power'] = required_power

			return f(*args, **kwargs)
		
		return decorated

	return wrapped

# Current user needs to meet the power requirements to pass
# Adds current_user, current_user_power and required_power to keyword arguments
def power_required(required_power):
	def wrapped(f):
		@wraps(f)
		@get_current_user
		def decorated(*args, **kwargs):
			# get current user form decorater
			current_user = kwargs['current_user']

			# Error if no current user is provided (should not happen due to exception handling in decorater)
			if not current_user:
				raise InternalServerError
			
			# Power of the current user
			current_user_power = current_user.user_role_br.power

			# Error if the users power is lower than the required power
			if current_user_power < required_power:
				raise ForbiddenError
			
			# Remove keyword argument 'current_user' if not needed in decorated function
			if not 'current_user' in inspect.getfullargspec(f).args:
				del kwargs['current_user']

			return f(*args, **kwargs)
		
		return decorated

	return wrapped


# Current user needs to match a given public_user_id or meet the power requirements to pass
# Adds current_user, current_user_power and required_power to keyword arguments
def power_or_auth_required(required_power):
	def wrapped(f):
		@wraps(f)
		@get_current_user
		def decorated(*args, **kwargs):
			current_user = kwargs['current_user']

			# First whether the logged in user corresponds to the requested user
			if 'user_id' in inspect.getfullargspec(f).args:
				if current_user.public_user_id == kwargs['user_id']:
					# Set keyword arguments according to decorated function
					if not 'current_user' in inspect.getfullargspec(f).args:
						del kwargs['current_user']
					if 'required_power' in inspect.getfullargspec(f).args:
						kwargs['required_power'] = required_power

					return f(*args, **kwargs)
			
			# If the users dont match check power requirements
			if not current_user.user_role_br.power < required_power:
				# Set keyword arguments according to decorated function
				if not 'current_user' in inspect.getfullargspec(f).args:
					del kwargs['current_user']
				if 'required_power' in inspect.getfullargspec(f).args:
					kwargs['required_power'] = required_power

				return f(*args, **kwargs)

			raise ForbiddenError
		
		return decorated

	return wrapped


# Login route for the api
class Login(Resource):
	def get(self):
		# Auth request provides username and password
		# In our case the username is the email
		auth = request.authorization

		# Both username and passowrd must be given in the request
		if not auth or not auth.username or not auth.password:
			raise UnauthorizedError

		from application.database.models import User
		user = User.query.filter_by(email=auth.username).first()

		# Exception if there is no user matching the given email
		if not user:
			raise NotFoundError

		# Password check
		if check_password_hash(user.password, auth.password):
			# Token is created with public user id and expiry date/time
			token = jwt.encode({'public_user_id' : user.public_user_id, 'power' : user.user_role_br.power, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=300)}, app.config['SECRET_KEY'], algorithm='HS256')

			return {'token' : token.decode('UTF-8')}, 200
		
		# Exception if the password is wrong
		raise UnauthorizedError
