""" Api endpoints for bookings """

__author__ = "Markus Mogck"
__version__ = "1.0"

import uuid
import datetime

from flask import current_app as app
from flask import jsonify, make_response, request
from flask_restful import Resource
from werkzeug.security import check_password_hash, generate_password_hash

from application.database.models import User, Vehicle, BookingStatus, Booking, db
from application.resources.api_auth import (get_current_user,
											power_or_auth_required,
											power_required,
											get_foreign_add_power)
from application.resources.errors import *


class BookingsApi(Resource):

	# Method:   GET
	# Usage:    get all bookings
	# Power:    3
	#
	# Users can access their bookings via the UserBookingApi / UserBookingsApi
	@power_required(3)
	def get(self):
		bookings = Booking.query.all()

		output = []

		for booking in bookings:
			booking_data = {}
			booking_data['public_booking_id'] = booking.public_booking_id
			booking_data['public_user_id'] = booking.user_br.public_user_id
			booking_data['public_vehicle_id'] = booking.vehicle_br.public_vehicle_id
			booking_data['start_date'] = booking.start_date
			booking_data['end_date'] = booking.end_date
			booking_data['booking_date'] = booking.booking_date
			booking_data['status'] = booking.status_br.status
			booking_data['fee'] = booking.fee
			booking_data['comment'] = booking.comment

			output.append(booking_data)

		return make_response(jsonify(output), 200)


	# Method:   POST
	# Usage:    adding a new booking
	# Power:    1 (3 if you add a booking for a foreign account)
	@power_required(1)
	@get_foreign_add_power(3)
	def post(self, current_user, foreign_add_power):
		data = request.get_json()

		# The ID must be contained in the request and is not taken from the sending user
		# This way a booking can also be entered by an employee
		try:
			if not data['public_user_id'] \
				or not data['public_vehicle_id'] \
				or not data['start_date'] \
				or not data['end_date'] \
				or not data['fee'] \
				or not data['status']:
				# Raise an error if the fields exist but are not filled
				raise BadRequestError
		except KeyError:
			# Raise an error if the fields dont exist in the request
			raise BadRequestError

		# Make sure that a booking can only be added by the current user
		# or by a user with appropriate rights
		if not data['public_user_id'] == current_user.public_user_id:
			if current_user.user_role_br.power < foreign_add_power:
				raise UnauthorizedError
		
		try:
			vehicle_id = Vehicle.query.filter_by(public_vehicle_id=data['public_vehicle_id']).first().id
			user_id = User.query.filter_by(public_user_id=data['public_user_id']).first().id
			status_id = BookingStatus.query.filter_by(status=data['status']).first().id
		except Exception:
			raise BadRequestError

		try:
			new_booking = Booking(
				public_booking_id=str(uuid.uuid4()),
				user_id=user_id,
				vehicle_id=vehicle_id,
				start_date=datetime.datetime.strptime(data['start_date'], '%Y-%m-%dT%H:%M:%S.%fZ'),
				end_date=datetime.datetime.strptime(data['end_date'], '%Y-%m-%dT%H:%M:%S.%fZ'),
				status_id=status_id,
				fee=data['fee'],
				comment=data['comment'])
		except Exception:
			# Raise an error if the provided data does not fit the database standard
			raise BadRequestError

		db.session.add(new_booking)
		db.session.commit()

		return {"message" : "New booking created!"}, 201


	# Method:   DELETE
	# Usage:    deletes all bookings
	# Power:    6
	@power_required(6)
	def delete(self):
		bookings = Booking.query.all()

		for booking in bookings:
			db.session.delete(booking)

		db.session.commit()

		return {'message' : 'All bookings has been deleted!'}, 204



class BookingApi(Resource):

	# Method:   GET
	# Usage:    get a single booking
	# Power:    3
	#
	# Users can access their bookings via the UserBookingApi / UserBookingsApi
	# TODO: find a better solution (should)
	@power_required(3)
	def get(self, booking_id):

		booking = Booking.query.filter_by(public_booking_id=booking_id).first()

		if not booking:
			raise NotFoundError

		booking_data = {}
		booking_data['public_booking_id'] = booking.public_booking_id
		booking_data['public_user_id'] = booking.user_br.public_user_id
		booking_data['public_vehicle_id'] = booking.vehicle_br.public_vehicle_id
		booking_data['start_date'] = booking.start_date
		booking_data['end_date'] = booking.end_date
		booking_data['booking_date'] = booking.booking_date
		booking_data['status'] = booking.status_br.status
		booking_data['fee'] = booking.fee
		booking_data['comment'] = booking.comment

		return make_response(jsonify(booking_data), 200)


	# Method:   PUT
	# Usage:    updating a single booking
	# Power:    3
	@power_required(3)
	def put(self, booking_id, **kwargs):
		from contextlib import suppress

		data = request.get_json()
		booking_to_update = Booking.query.filter_by(public_booking_id=booking_id).first()

		if not booking_to_update:
			raise NotFoundError

		try:
			with suppress(KeyError): booking_to_update.vehicle_id = Vehicle.query.filter_by(public_vehicle_id=data['public_vehicle_id']).first().id
			with suppress(KeyError): booking_to_update.start_date= datetime.datetime.strptime(data['start_date'], '%Y-%m-%dT%H:%M:%S.%fZ')
			with suppress(KeyError): booking_to_update.end_date= datetime.datetime.strptime(data['end_date'], '%Y-%m-%dT%H:%M:%S.%fZ')
			with suppress(KeyError): booking_to_update.status_id = BookingStatus.query.filter_by(status=data['status']).first().id
			with suppress(KeyError): booking_to_update.fee = data['fee']
			with suppress(KeyError): booking_to_update.comment = data['comment']
		except Exception:
			raise BadRequestError

		db.session.commit()

		return {'message' : 'The booking has been updated!'}, 200


	# Method:   DELETE
	# Usage:    delete a single booking
	# Power:    6
	@power_required(6)
	def delete(self, booking_id):
		booking = Booking.query.filter_by(public_booking_id=booking_id).first()

		if not booking:
			raise NotFoundError

		db.session.delete(booking)
		db.session.commit()

		return {'message' : 'The booking has been deleted!'}, 204



class UserBookingsApi(Resource):

	# Method:   GET
	# Usage:    get all bookings from a specified user
	# Power:    3
	@power_or_auth_required(3)
	def get(self, user_id):
		user = User.query.filter_by(public_user_id=user_id).first()

		if not user:
			raise NotFoundError

		bookings = Booking.query.filter_by(user_id=user.id).all()

		output = []

		for booking in bookings:
			booking_data = {}
			booking_data['public_booking_id'] = booking.public_booking_id
			booking_data['public_user_id'] = booking.user_br.public_user_id
			booking_data['public_vehicle_id'] = booking.vehicle_br.public_vehicle_id
			booking_data['start_date'] = booking.start_date
			booking_data['end_date'] = booking.end_date
			booking_data['booking_date'] = booking.booking_date
			booking_data['status'] = booking.status_br.status
			booking_data['fee'] = booking.fee
			booking_data['comment'] = booking.comment

			output.append(booking_data)

		return make_response(jsonify(output), 200)



class UserBookingApi(Resource):

	# Method:   GET
	# Usage:    get a single booking from a specified user
	# Power:    3
	@power_or_auth_required(3)
	def get(self, user_id, booking_id, current_user, required_power):
		user = User.query.filter_by(public_user_id=user_id).first()
		booking = Booking.query.filter_by(public_booking_id=booking_id).first()

		if not booking or not user:
			raise NotFoundError
		
		# U can only recieve a booking with appropriate right or if they are your bookings
		if current_user.user_role_br.power < required_power:
			if user.id != booking.user_id:
				raise ForbiddenError

		booking_data = {}
		booking_data['public_booking_id'] = booking.public_booking_id
		booking_data['public_user_id'] = booking.user_br.public_user_id
		booking_data['public_vehicle_id'] = booking.vehicle_br.public_vehicle_id
		booking_data['start_date'] = booking.start_date
		booking_data['end_date'] = booking.end_date
		booking_data['booking_date'] = booking.booking_date
		booking_data['status'] = booking.status_br.status
		booking_data['fee'] = booking.fee
		booking_data['comment'] = booking.comment

		return make_response(jsonify(booking_data), 200)



class UserBookingCancelApi(Resource):

	# Method:   PUT
	# Usage:    cancels a single booking
	# Power:    3 or being the user to be edited
	@power_or_auth_required(3)
	def put(self, user_id, booking_id, current_user, required_power):
		user = User.query.filter_by(public_user_id=user_id).first()
		booking = Booking.query.filter_by(public_booking_id=booking_id).first()

		if not booking or not user:
			raise NotFoundError
		
		if current_user.user_role_br.power < required_power:
			if user.id != booking.user_id:
				raise ForbiddenError

		booking.status_id = BookingStatus.query.filter_by(status='storniert').first().id
		db.session.commit()

		return {'message' : 'The booking has been canceled!'}, 200