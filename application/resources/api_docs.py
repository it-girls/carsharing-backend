""" This file provides the path to the swagger file """

__author__ = "Markus Mogck"
__version__ = "1.0"

from flask import current_app as app
from flask import jsonify, make_response, request
from flask_restful import Resource

from application.resources.errors import *


class Docs(Resource):
	def get(self):
		import json
		from os import path
		from config import basedir

		swagger_path = path.join(basedir, './static/swagger.json')

		with open(swagger_path) as json_file:
			data = json.load(json_file)
			return make_response(data, 200)