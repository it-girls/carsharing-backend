""" Endpoint to initialize test values """

__author__ = "Markus Mogck"
__version__ = "1.0"

from flask import request, jsonify, make_response
from flask_restful import Resource
from application.resources.errors import *

from application.database.db_init_values import initialize_values
from application.database.db_init_values_test import initialize_test_values

class InitTestValues(Resource):
    def get(self):
        initialize_test_values()
        return make_response(jsonify(), 200)
