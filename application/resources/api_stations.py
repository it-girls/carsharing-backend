""" Api endpoints for the stations """

__author__ = "Markus Mogck"
__version__ = "1.0"

import uuid

from flask import current_app as app
from flask import jsonify, make_response, request
from flask_restful import Resource

from application.database.models import Station, db
from application.resources.api_auth import (get_current_user,
											power_or_auth_required,
											power_required)
from application.resources.errors import *


class StationsApi(Resource):
	def get(self):
		stations = Station.query.all()

		output = []

		for station in stations:
			station_data = {}
			station_data['public_station_id'] = station.public_station_id
			station_data['name'] = station.name
			station_data['address'] = station.address
			station_data['post_code'] = station.post_code
			station_data['city'] = station.city
			station_data['country'] = station.country

			output.append(station_data)

		return make_response(jsonify(output), 200)


	@power_required(6)
	def post(self):
		data = request.get_json()

		try:
			if not data['name']:
				# name is empty
				raise BadRequestError
		except KeyError:
			# Raise an error if the fields dont exist in the request
			raise BadRequestError
		
		check_station_name = Station.query.filter_by(name=data['name']).first()

		if check_station_name:
			# Error if station name is already used
			raise BadRequestError
		
		new_station = Station(
			public_station_id=str(uuid.uuid4()),
			name=data['name'],
			address=data['address'],
			post_code=data['post_code'],
			city=data['city'],
			country=data['country'])
		
		db.session.add(new_station)
		db.session.commit()

		return {"message" : "New station created!"}, 201


	@power_required(6)
	def delete(self):
		try:
			stations = Station.query.all()

			for station in stations:
				db.session.delete(station)

			db.session.commit()

			return {'message' : 'All stations has been deleted!'}, 204
		except Exception:
			# Raise an error if an unexpected error occures
			raise InternalServerError


class StationApi(Resource):
	def get(self, station_id):
		station = Station.query.filter_by(public_station_id=station_id).first()

		if not station:
			raise NotFoundError

		station_data = {}
		station_data['public_station_id'] = station.public_station_id
		station_data['name'] = station.name
		station_data['address'] = station.address
		station_data['post_code'] = station.post_code
		station_data['city'] = station.city
		station_data['country'] = station.country

		return make_response(jsonify(station_data), 200)
	
	@power_required(6)
	def put(self, station_id):
		from contextlib import suppress

		data = request.get_json()
		station_to_update = Station.query.filter_by(public_station_id=station_id).first()

		if not station_to_update:
			raise NotFoundError

		# Update Name (unique)
		new_station_name = None
		with suppress(KeyError): new_station_name = data['name']
		if new_station_name is not None:
			station_query = Station.query.filter_by(name=new_station_name).first()
			if not station_query:
				station_to_update.name = new_station_name
			else:
				raise ConflictError

		# Basic updates
		# Suppresses must be seperate lines because when the first exception is raised, the other updates will no longer be performed
		with suppress(KeyError): station_to_update.address = data['address']
		with suppress(KeyError): station_to_update.post_code = data['post_code']
		with suppress(KeyError): station_to_update.city = data['city']
		with suppress(KeyError): station_to_update.country = data['country']

		db.session.commit()

		return {'message' : 'The station has been updated!'}, 200

	@power_required(6)
	def delete(self, station_id):
		station = Station.query.filter_by(public_station_id=station_id).first()

		if not station:
			raise NotFoundError

		db.session.delete(station)
		db.session.commit()

		return {'message' : 'The station has been deleted!'}, 204


class StationVehiclesApi(Resource):
	def get(self, station_id):
		station = Station.query.filter_by(public_station_id=station_id).first()

		if not station:
			raise NotFoundError

		station_vehicles = station.vehicle

		output = []

		for vehicle in station_vehicles:
			vehicle_data = {}
			vehicle_data['public_vehicle_id'] = vehicle.public_vehicle_id
			vehicle_data['license_plate'] = vehicle.license_plate
			vehicle_data['station_name'] = vehicle.station_br.name
			vehicle_data['comment'] = vehicle.comment
			vehicle_data['brand'] = vehicle.brand
			vehicle_data['model'] = vehicle.model
			vehicle_data['category_flag'] = vehicle.category_br.flag
			vehicle_data['category_description'] = vehicle.category_br.description
			vehicle_data['vehicle_type_flag'] = vehicle.vehicle_type_br.flag
			vehicle_data['vehicle_type_description'] = vehicle.vehicle_type_br.description
			vehicle_data['transmission_flag'] = vehicle.transmission_br.flag
			vehicle_data['transmission_description'] = vehicle.transmission_br.description
			vehicle_data['addition_flag'] = vehicle.addition_br.flag
			vehicle_data['addition_description'] = vehicle.addition_br.description

			output.append(vehicle_data)

		return make_response(jsonify(output), 200)