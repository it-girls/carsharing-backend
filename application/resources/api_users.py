""" Login path and authorization decoraters """

__author__ = "Markus Mogck"
__version__ = "1.0"

import uuid
import datetime

from flask import current_app as app
from flask import jsonify, make_response, request
from flask_restful import Resource
from werkzeug.security import check_password_hash, generate_password_hash

from application.database.models import User, UserRole, db
from application.resources.api_auth import (get_current_user,
											power_or_auth_required,
											power_required)
from application.resources.errors import *


class UsersApi(Resource):

	# Method:   GET
	# Usage:    get all users
	# Power:    3
	@power_required(3)
	def get(self):
		users = User.query.all()

		output = []

		for user in users:
			user_data = {}
			user_data['public_user_id'] = user.public_user_id
			user_data['email'] = user.email
			user_data['password'] = user.password
			user_data['user_role'] = user.user_role_br.role
			user_data['creation_date'] = user.creation_date
			user_data['first_name'] = user.first_name
			user_data['last_name'] = user.last_name
			user_data['birthdate'] = user.birthdate
			user_data['id_number'] = user.id_number
			user_data['license_number'] = user.license_number

			output.append(user_data)
		
		return make_response(jsonify(output), 200)


	# Method:   POST
	# Usage:    create a new user
	# Power:    none
	def post(self):
		data = request.get_json()
		
		try:
			if not data['email'] or not data['password']:
				# Raise an error if the fields exist but are not filled
				raise BadRequestError
		except Exception:
			# Raise an error if the fields dont exist in the request
			raise BadRequestError
		
		# Checks whether the address already exists
		if not User.query.filter_by(email=data['email']).first() == None:
			raise BadRequestError

		hashed_password = generate_password_hash(data['password'], method='sha256')
		
		try:
			new_user = User(
				public_user_id=str(uuid.uuid4()),
				email=data['email'],
				password=hashed_password,
				# To change user role use put method after creation
				user_role_id=UserRole.query.filter_by(role='benutzer').first().id,
				first_name=data['first_name'],
				last_name=data['last_name'],
				birthdate=datetime.datetime.strptime(data['birthdate'], '%Y-%m-%dT%H:%M:%S.%fZ'),
				id_number=data['id_number'],
				license_number=data['license_number'])
		except Exception:
			# Raise an error if the provided data does not fit the database standard
			raise BadRequestError
		
		db.session.add(new_user)
		db.session.commit()

		return {"message" : "New user created!"}, 201


	# Method:   DELETE
	# Usage:    delete all users
	# Power:    6
	@power_required(6)
	def delete(self):
		try:
			users = User.query.all()

			for user in users:
				db.session.delete(user)

			db.session.commit()

			return {'message' : 'All users has been deleted!'}, 204
		except Exception:
			# Raise an error if an unexpected error occures
			# Raised on IntegrityCheck
			raise InternalServerError


class UserApi(Resource):
	
	# Method:   Get
	# Usage:    get a single user
	# Power:    3 or authorization
	@power_or_auth_required(3)
	def get(self, user_id):
		user = User.query.filter_by(public_user_id=user_id).first()

		if not user:
			raise NotFoundError

		user_data = {}
		user_data['public_user_id'] = user.public_user_id
		user_data['email'] = user.email
		user_data['password'] = user.password
		user_data['user_role'] = user.user_role_br.role
		user_data['creation_date'] = user.creation_date
		user_data['first_name'] = user.first_name
		user_data['last_name'] = user.last_name
		user_data['birthdate'] = user.birthdate
		user_data['id_number'] = user.id_number
		user_data['license_number'] = user.license_number

		return make_response(jsonify(user_data), 200)
	

	# Method:   PUT
	# Usage:    update a single user
	# Power:    3 or authorization
	@power_or_auth_required(3)
	def put(self, user_id, current_user):
		from contextlib import suppress

		data = request.get_json()
		user_to_update = User.query.filter_by(public_user_id=user_id).first()

		if not user_to_update:
			raise NotFoundError

		# Basic updates
		# Each Suppresses must be seperate lines because when the first exception is raised,
		# the other updates will no longer be performed
		with suppress(KeyError): user_to_update.first_name = data['first_name']
		with suppress(KeyError): user_to_update.last_name = data['last_name']
		with suppress(KeyError): user_to_update.id_number = data['id_number']
		with suppress(KeyError): user_to_update.license_number = data['license_number']

		# Email update
		new_email = None
		with suppress(Exception): new_email = data['email']
		# Update Email if email key is provided in the request
		if new_email is not None:
			# Check wetcher the new email already exists
			check_email = User.query.filter_by(email=new_email).first()
			if not check_email:
				user_to_update.email = new_email
			else:
				# Error if the user with the existing email is not the user to update
				if user_to_update != check_email:
					raise ConflictError

		# Password update
		new_password = None
		with suppress(Exception): new_password = data['password']
		# Update password if password key is provided in the request
		if new_password is not None:
			user_to_update.password = generate_password_hash(new_password, method='sha256')

		# Role updates
		new_user_role = None
		with suppress(Exception): new_user_role = data['user_role']
		if new_user_role is not None:
			try:
				new_user_role_power = UserRole.query.filter_by(role=new_user_role).first().power
				new_user_role_id = UserRole.query.filter_by(role=new_user_role).first().id
			except Exception:
				raise BadRequestError
			# New power has to be equal or lower than the own power
			# (employee cant assign admin rights)        
			if current_user.user_role_br.power < new_user_role_power:
				raise UnauthorizedError
			else:
				user_to_update.user_role_id = new_user_role_id

		db.session.commit()

		return {'message' : 'The user has been updated!'}, 200


	# Method:   DELETE
	# Usage:    deletes a single user
	# Power:    6
	@power_required(6)
	def delete(self, user_id):
		user = User.query.filter_by(public_user_id=user_id).first()

		if not user:
			raise NotFoundError

		db.session.delete(user)
		db.session.commit()

		return {'message' : 'The user has been deleted!'}, 204
