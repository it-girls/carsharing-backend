""" Api endpoints for vehicles """

__author__ = "Markus Mogck"
__version__ = "1.0"

import uuid

from flask import current_app as app
from flask import jsonify, make_response, request
from flask_restful import Resource

from application.database.models import Vehicle, Station, db
from application.resources.api_auth import (get_current_user,
											power_or_auth_required,
											power_required,
											get_foreign_add_power)
from application.resources.errors import *


class VehiclesApi(Resource):
	def get(self):
		vehicles = Vehicle.query.all()

		output = []

		for vehicle in vehicles:
			vehicle_data = {}
			vehicle_data['public_vehicle_id'] = vehicle.public_vehicle_id
			vehicle_data['license_plate'] = vehicle.license_plate
			vehicle_data['public_station_id'] = vehicle.station_br.public_station_id
			vehicle_data['station_name'] = vehicle.station_br.name
			vehicle_data['comment'] = vehicle.comment
			vehicle_data['brand'] = vehicle.brand
			vehicle_data['model'] = vehicle.model
			vehicle_data['category_flag'] = vehicle.category_br.flag
			vehicle_data['category_description'] = vehicle.category_br.description
			vehicle_data['vehicle_type_flag'] = vehicle.vehicle_type_br.flag
			vehicle_data['vehicle_type_description'] = vehicle.vehicle_type_br.description
			vehicle_data['transmission_flag'] = vehicle.transmission_br.flag
			vehicle_data['transmission_description'] = vehicle.transmission_br.description
			vehicle_data['addition_flag'] = vehicle.addition_br.flag
			vehicle_data['addition_description'] = vehicle.addition_br.description

			output.append(vehicle_data)

		return make_response(jsonify(output), 200)

	@power_required(6)
	def post(self):
		data = request.get_json()
		
		try:
			if not data['chassis_number']:
				# chassis number empty
				raise BadRequestError
		except KeyError:
			# no chassis number provided
			raise BadRequestError
		
		check_chassis_number = Vehicle.query.filter_by(chassis_number=data['chassis_number']).first()
		if check_chassis_number:
			# Chassis number already exists
			raise BadRequestError
		
		try:
			station_id = Station.query.filter_by(public_station_id=data['public_station_id']).first().id
		except Exception:
			# Station does not exist
			raise BadRequestError

		try:
			from application.database.models import VehicleAddition, VehicleCategorie, VehicleTransmission, VehicleType
			new_category = VehicleCategorie.query.filter_by(flag=data['category']).first().flag
			new_vehicle_type = VehicleType.query.filter_by(flag=data['vehicle_type']).first().flag
			new_transmission = VehicleTransmission.query.filter_by(flag=data['transmission']).first().flag
			new_addition = VehicleAddition.query.filter_by(flag=data['addition']).first().flag
		except Exception:
			raise BadRequestError

		new_vehicle = Vehicle(
			public_vehicle_id=str(uuid.uuid4()),
			chassis_number=data['chassis_number'],
			license_plate=data['license_plate'],
			station_id=station_id,
			comment=data['comment'],
			brand=data['brand'],
			model=data['model'],
			category=new_category,
			vehicle_type=new_vehicle_type,
			transmission=new_transmission,
			addition=new_addition)
		
		db.session.add(new_vehicle)
		db.session.commit()

		return {"message" : "New vehicle created!"}, 201


	@power_required(6)
	def delete(self):
		try:
			vehicles = Vehicle.query.all()

			for vehicle in vehicles:
				db.session.delete(vehicle)

			db.session.commit()

			return {'message' : 'All vehicles has been deleted!'}, 204
		except Exception:
			# Raise an error if an unexpected error occures
			raise InternalServerError

class VehicleApi(Resource):
	def get(self, vehicle_id):
		vehicle = Vehicle.query.filter_by(public_vehicle_id=vehicle_id).first()

		if not vehicle:
			raise NotFoundError

		vehicle_data = {}
		vehicle_data['public_vehicle_id'] = vehicle.public_vehicle_id
		vehicle_data['license_plate'] = vehicle.license_plate
		vehicle_data['public_station_id'] = vehicle.station_br.public_station_id
		vehicle_data['station_name'] = vehicle.station_br.name
		vehicle_data['comment'] = vehicle.comment
		vehicle_data['brand'] = vehicle.brand
		vehicle_data['model'] = vehicle.model
		vehicle_data['category_flag'] = vehicle.category_br.flag
		vehicle_data['category_description'] = vehicle.category_br.description
		vehicle_data['vehicle_type_flag'] = vehicle.vehicle_type_br.flag
		vehicle_data['vehicle_type_description'] = vehicle.vehicle_type_br.description
		vehicle_data['transmission_flag'] = vehicle.transmission_br.flag
		vehicle_data['transmission_description'] = vehicle.transmission_br.description
		vehicle_data['addition_flag'] = vehicle.addition_br.flag
		vehicle_data['addition_description'] = vehicle.addition_br.description

		return make_response(jsonify(vehicle_data), 200)
	
	@power_or_auth_required(3)
	def put(self, vehicle_id):
		from contextlib import suppress

		data = request.get_json()
		vehicle_to_update = Vehicle.query.filter_by(public_vehicle_id=vehicle_id).first()

		if not vehicle_to_update:
			raise NotFoundError

		# Basic updates
		# Suppresses must be seperate lines because when the first exception is raised, the other updates will no longer be performed
		with suppress(KeyError): vehicle_to_update.license_plate = data['license_plate']
		with suppress(KeyError): vehicle_to_update.station_id = Station.query.filter_by(public_station_id=data['public_station_id']).first().id
		with suppress(KeyError): vehicle_to_update.comment = data['comment']
		with suppress(KeyError): vehicle_to_update.brand = data['brand']
		with suppress(KeyError): vehicle_to_update.model = data['model']
		with suppress(KeyError): vehicle_to_update.category = data['category']
		with suppress(KeyError): vehicle_to_update.vehicle_type = data['vehicle_type']
		with suppress(KeyError): vehicle_to_update.transmission = data['transmission']
		with suppress(KeyError): vehicle_to_update.addition = data['addition']

		db.session.commit()

		return {'message' : 'The vehicle has been updated!'}, 200

	@power_required(6)
	def delete(self, vehicle_id):
		vehicle = Vehicle.query.filter_by(public_vehicle_id=vehicle_id).first()

		if not vehicle:
			raise NotFoundError

		db.session.delete(vehicle)
		db.session.commit()

		return {'message' : 'The vehicle has been deleted!'}, 204
