""" This file contains all used api errors and their descriptions """

__author__ = "Markus Mogck"
__version__ = "1.0"

class InternalServerError(Exception):
    pass

class NotImplementedError(Exception):
    pass

class BadRequestError(Exception):
    pass

class UnauthorizedError(Exception):
    pass

class ForbiddenError(Exception):
    pass

class NotFoundError(Exception):
    pass

class MethodNotAllowedError(Exception):
    pass

class NotAcceptableError(Exception):
    pass

class RequestTimeoutError(Exception):
    pass

class ConflictError(Exception):
    pass

class GoneError(Exception):
    pass

class RequestEntityTooLargeError(Exception):
    pass

errors = {
    "InternalServerError": {
        "status": 500,
        "message": "Something went wrong"
    },
    "NotImplementedError": {
        "status": 500,
        "message": "The functionality to process the request is not provided by this server"
    },
    "BadRequestError": {
        "status": 400,
        "message": "Malformed request syntax"
    },
    "UnauthorizedError": {
        "status": 401,
        "message": "Authentication has failed or has not yet been provided"
    },
    "ForbiddenError": {
        "status": 403,
        "message": "Server is refusing action due to not having the necessary permissions"
    },
    "NotFoundError": {
        "status": 404,
        "message": "The requested resource could not be found but may be available in the future"
    },
    "MethodNotAllowedError": {
        "status": 405,
        "message": "This request method is not supported for the requested resource"
    },
    "NotAcceptableError": {
        "status": 406,
        "message": ""
    },
    "RequestTimeoutError": {
        "status": 408,
        "message": ""
    },
    "ConflictError": {
        "status": 409,
        "message": ""
    },
    "GoneError": {
        "status": 410,
        "message": ""
    },
    "RequestEntityTooLargeError": {
        "status": 413,
        "message": ""
    }
}