""" Initialization of all api routes """

__author__ = "Markus Mogck"
__version__ = "1.0"

from application.resources.api_auth import Login
from application.resources.api_users import UserApi, UsersApi
from application.resources.api_bookings import BookingApi, BookingsApi, UserBookingApi, UserBookingsApi, UserBookingCancelApi
from application.resources.api_stations import StationApi, StationsApi, StationVehiclesApi
from application.resources.api_vehicles import VehicleApi, VehiclesApi
from application.resources.api_init import InitTestValues
from application.resources.api_docs import Docs

from flask_swagger_ui import get_swaggerui_blueprint
from flask import current_app as app


def initialize_routes(api):
    api.add_resource(Login, "/api/login")

    api.add_resource(UsersApi, "/api/users")
    api.add_resource(UserApi, "/api/users/<user_id>")
    api.add_resource(UserBookingsApi, "/api/users/<user_id>/bookings")
    api.add_resource(UserBookingApi, "/api/users/<user_id>/bookings/<booking_id>")
    api.add_resource(UserBookingCancelApi, "/api/users/<user_id>/bookings/<booking_id>/cancel")

    api.add_resource(BookingsApi, "/api/bookings")
    api.add_resource(BookingApi, "/api/bookings/<booking_id>")

    api.add_resource(StationsApi, "/api/stations")
    api.add_resource(StationApi, "/api/stations/<station_id>")
    api.add_resource(StationVehiclesApi, "/api/stations/<station_id>/vehicles")

    api.add_resource(VehiclesApi, "/api/vehicles")
    api.add_resource(VehicleApi, "/api/vehicles/<vehicle_id>")

    api.add_resource(InitTestValues, "/api/init/test")
    
    api.add_resource(Docs, "/api/docs/swagger.json")


def initialize_swagger(app):

    SWAGGER_URL = '/api/docs'
    API_URL = '/api/docs/swagger.json'
    swaggerui_blueprint = get_swaggerui_blueprint(
        SWAGGER_URL,
        API_URL,
        config={'app_name': "carsharing-backend"}
    )
    app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization,x-access-token,apiKey,api_key,jwt')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    response.headers.add('Access-Control-Allow-Credentials', 'true')
    return response