""" Configuration file with Production, Development and Test config """

__author__ = "Markus Mogck"
__version__ = "1.0"

from os import environ, path
from dotenv import load_dotenv
from application.database import db_base_path


basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))


APP_HOST = '0.0.0.0'
APP_PORT = '5002'
APP_SSL = None

# Base Configuration
class Config:
	FLASK_APP = 'wsgi.py'
	SECRET_KEY = environ.get('SECRET_KEY')
	FLASK_SECRET = SECRET_KEY
	JSON_SORT_KEYS = False


# Production Configuration
class ProdConfig(Config):
	FLASK_ENV = 'production'

	DEVELOPMENT = False
	DEBUG = False
	TESTING = False

	SQLALCHEMY_DATABASE_URI = "sqlite:///"  + db_base_path + '/db_prod.sqlite3'
	SQLALCHEMY_ECHO = False
	#SQLALCHEMY_ENGINE_OPTIONS = ''
	SQLALCHEMY_TRACK_MODIFICATIONS = True


# Development Configuration
class DevConfig(Config):
	FLASK_ENV = 'development'

	DEVELOPMENT = True
	DEBUG = True
	TESTING = True

	SQLALCHEMY_DATABASE_URI = 'sqlite:///'  + db_base_path + '/db_dev.sqlite3'
	SQLALCHEMY_ECHO = True
	#SQLALCHEMY_ENGINE_OPTIONS = ''
	SQLALCHEMY_TRACK_MODIFICATIONS = True


# Test Configuration
class TestConfig(Config):
	FLASK_ENV = 'development'

	DEVELOPMENT = True
	DEBUG = False
	TESTING = True

	SQLALCHEMY_DATABASE_URI = 'sqlite:///'  + db_base_path + '/db_test.sqlite3'
	SQLALCHEMY_ECHO = False
	#SQLALCHEMY_ENGINE_OPTIONS = ''
	SQLALCHEMY_TRACK_MODIFICATIONS = True