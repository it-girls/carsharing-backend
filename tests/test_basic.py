""" Basic and authorization tests """

__author__ = "Markus Mogck"
__version__ = "1.0"

import os
import unittest
 
from flask import current_app as app
from application import db

from application.resources.errors import *


class BasicTests(unittest.TestCase):

	###############################################################################################

	# executed prior to each test
	def setUp(self):
		# Set up the app
		from application import create_app
		from config import TestConfig
		app = create_app(config=TestConfig)

		# Clear and re-initialize the test database
		from application.database.db_init_values_test import initialize_test_values
		from application.database.db_init_values import initialize_admin, initialize_values
		with app.app_context():
			db.drop_all()
			db.create_all()
			initialize_values()
			initialize_admin()
			initialize_test_values()

		# Initialize simulated Client
		self.app = app.test_client()
 
		self.assertEqual(app.debug, False)

		# Set jwt tokens for later use / tests
		self.admin_token = self.get_token('admin', 'admin')
		self.employee_token = self.get_token('employee1', 'employee1')
		self.user_token = self.get_token('user1', 'user1')

	# executed after each test
	def tearDown(self):
		pass

	# Helper method to login
	def login(self, username, password):
		import base64
		return self.app.get(
			'/api/login',
			headers={'Authorization': 'Basic ' + base64.b64encode(bytes(username + ":" + password, 'ascii')).decode('ascii')},
			follow_redirects=True
		)
	
	# Helper method to login that returns only the jwt token
	def get_token(self, username, password):
		import json
		response = self.login(username, password)
		self.assertIn(b'token', response.data)
		data = json.loads(response.get_data(as_text=True))
		return data['token']
		
	###############################################################################################

	def test_login_authorized(self):
		response = self.login('admin', 'admin')
		self.assertEqual(response.status_code, 200)
		self.assertIn(b'token', response.data)
	
	def test_login_unautorized(self):
		try:
			response = self.login('admin', 'wrong_admin_pw')
		except Exception as exception:
			self.assertIsInstance(exception, UnauthorizedError)

	def test_login_no_data(self):
		try:
			response = self.login('', '')
		except Exception as exception:
			self.assertIsInstance(exception, UnauthorizedError)

	def test_login_no_user(self):
		try:
			response = self.login('wrong_admin_user', 'admin')
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)

	def test_login_tokens(self):
		self.assertTrue(self.admin_token)
		self.assertTrue(self.employee_token)
		self.assertTrue(self.user_token)
	
	def test_get_all_users_no_header(self):
		try:
			response = self.app.get(
				'/api/users',
				headers={},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, UnauthorizedError)
	
	def test_get_all_users_token_expired(self):
		try:
			response = self.app.get(
				'/api/users',
				headers={'x-access-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwdWJsaWNfdXNlcl9pZCI6IjQ3MWJmZDEyLTc1ZjEtXXc4MC1iYzY4LWQyMzIwYjI3MTJhOSIsImV4cCI6MTU5MTY2NjM5MX0.YRYnU1sgZh7mYS96AeAFbcdPxzVLI0mhGeY70OawjyI'},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)
	
	def test_get_all_users_user_not_found(self):
		try:
			test_token = self.get_token('employee2', 'employee2')
			response_delete_user2 = self.app.delete(
				'/api/users/employee2_uuid',
				headers={'x-access-token': self.admin_token},
				content_type='application/json',
				follow_redirects=True
			)
			response_token_expired = self.app.get(
				'/api/users',
				headers={'x-access-token': test_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)

	def test_docs_page(self):
		response = self.app.get('/api/docs', follow_redirects=True)
		self.assertEqual(response.status_code, 200)

	def test_docs_file(self):
		response = self.app.get('/api/docs/swagger.json', follow_redirects=True)
		self.assertEqual(response.status_code, 200)
 
 
if __name__ == "__main__":
	unittest.main()