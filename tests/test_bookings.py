""" Testing booking endpoints """

__author__ = "Markus Mogck"
__version__ = "1.0"

import os
import unittest
import base64
import json
 
from flask import current_app as app
from application import db

from application.resources.errors import *


class BookingsTest(unittest.TestCase):

	###############################################################################################

	# executed prior to each test
	def setUp(self):
		# Set up the app
		from application import create_app
		from config import TestConfig
		app = create_app(config=TestConfig)

		# Clear and re-initialize the test database
		from application.database.db_init_values_test import initialize_test_values
		from application.database.db_init_values import initialize_admin, initialize_values
		with app.app_context():
			db.drop_all()
			db.create_all()
			initialize_values()
			initialize_admin()
			initialize_test_values()

		# Initialize simulated Client
		self.app = app.test_client()
 
		self.assertEqual(app.debug, False)

		# Set jwt tokens for later use / tests
		self.admin_token = self.get_token('admin', 'admin')
		self.employee_token = self.get_token('employee1', 'employee1')
		self.user_token = self.get_token('user1', 'user1')

	# executed after each test
	def tearDown(self):
		pass

	# Helper method to login
	def login(self, username, password):
		return self.app.get(
			'/api/login',
			headers={'Authorization': 'Basic ' + base64.b64encode(bytes(username + ":" + password, 'ascii')).decode('ascii')},
			follow_redirects=True
		)
	
	# Helper method to login that returns only the jwt token
	def get_token(self, username, password):
		import json
		response = self.login(username, password)
		self.assertIn(b'token', response.data)
		data = json.loads(response.get_data(as_text=True))
		return data['token']
		
	###############################################################################################

	def test_get_all_bookings_authorized(self):
		response = self.app.get(
			'/api/bookings',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)
	
	def test_get_all_bookings_forbidden(self):
		try:
			response = self.app.get(
				'/api/bookings',
				headers={'x-access-token': self.user_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, ForbiddenError)

	def test_post_booking(self):
		response = self.app.post(
			'/api/bookings',
			headers={'x-access-token': self.admin_token},
			data = json.dumps(
				dict(
					public_user_id='user1_uuid',
					public_vehicle_id='v1_uuid',
					start_date='2004-06-14T23:34:00.0000Z',
					end_date='2004-06-15T12:00:00.0000Z',
					status='aktiv',
					fee='50',
					comment='test comment'
				)
			),
			content_type='application/json',
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 201)

	def test_post_booking_unauthorized(self):
		try:
			response = self.app.post(
				'/api/bookings',
				headers={'x-access-token': self.user_token},
				data = json.dumps(
					dict(
						public_user_id='user2_uuid',
						public_vehicle_id='v1_uuid',
						start_date='2004-06-14Z23:34:00.0000Z',
						end_date='2004-06-15Z12:00:00.0000Z',
						status='aktiv',
						fee='50',
						comment='test comment'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, UnauthorizedError)

	def test_post_booking_no_data(self):
		try:
			response = self.app.post(
				'/api/bookings',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						public_user_id='',
						public_vehicle_id='',
						start_date='2004-06-14T23:34:00.0000Z',
						end_date='2004-06-15T12:00:00.0000Z',
						status='aktiv',
						fee='50',
						comment='test comment'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_post_booking_wrong_data1(self):
		try:
			response = self.app.post(
				'/api/bookings',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						public_user_id='user1_uuid',
						public_vehicle_id='v1_uuid',
						start_date='test',
						end_date='2004-06-15T12:00:00.0000Z',
						status='aktiv',
						fee='50',
						comment='test comment'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_post_booking_wrong_data2(self):
		try:
			response = self.app.post(
				'/api/bookings',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						start_date='2004-06-14T12:00:00.0000Z',
						end_date='2004-06-15T12:00:00.0000Z',
						status='aktiv',
						fee='50',
						comment='test comment'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_post_booking_wrong_data3(self):
		try:
			response = self.app.post(
				'/api/bookings',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						public_user_id='nonexistent',
						public_vehicle_id='nonexistent',
						start_date='2004-06-14T12:00:00.0000Z',
						end_date='2004-06-15T12:00:00.0000Z',
						status='nonexistent',
						fee='50',
						comment='test comment'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_delete_all_bookings_authorized(self):
		response = self.app.delete(
			'/api/bookings',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 204)

	def test_delete_all_bookings_forbidden(self):
		try:
			response = self.app.delete(
				'/api/bookings',
				headers={'x-access-token': self.user_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, ForbiddenError)

	def test_get_single_booking(self):
		response = self.app.get(
			'/api/bookings/b1_uuid',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)
	
	def test_get_single_booking_not_found(self):
		try:
			response = self.app.get(
				'/api/bookings/nonexistent_booking',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)

	def test_put_booking_new_status_and_comment(self):
		response = self.app.put(
			'/api/bookings/b1_uuid',
			headers={'x-access-token': self.admin_token},
			data = json.dumps(
				dict(
					public_vehicle_id='v2_uuid',
					start_date='2020-06-22T01:00:43.763Z',
					end_date='2020-06-22T02:00:43.763Z',
					status='abgeschlossen',
					fee='50',
					comment='new comment'
				)
			),
			content_type='application/json',
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)

	def test_put_booking_not_found(self):
		try:
			response = self.app.put(
				'/api/bookings/nonexistent_booking',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						public_vehicle_id='v2_uuid',
						start_date='2020-06-22T01:00:43.763Z',
						end_date='2020-06-22T02:00:43.763Z',
						status='abgeschlossen',
						fee='50',
						comment='new comment'
					)
				),
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)

	def test_put_booking_new_status_does_not_exist(self):
		try:
			response = self.app.put(
				'/api/bookings/b1_uuid',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						public_vehicle_id='v2_uuid',
						start_date='2020-06-22T01:00:43.763Z',
						end_date='2020-06-22T02:00:43.763Z',
						status='not_existing',
						fee='50',
						comment='new comment'
					)
				),
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_delete_booking(self):
		response = self.app.delete(
			'/api/bookings/b1_uuid',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 204)

	def test_delete_booking_not_found(self):
		try:
			response = self.app.delete(
				'/api/bookings/nonexistent_booking',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)

	def test_get_bookings_by_user(self):
		response = self.app.get(
			'/api/users/user1_uuid/bookings',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)
	
	def test_get_bookings_by_user_not_found(self):
		try:
			response = self.app.get(
				'/api/users/nonexistent_user/bookings',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)
			
	def test_get_single_booking_by_user(self):
		response = self.app.get(
			'/api/users/user1_uuid/bookings/b1_uuid',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)
	
	def test_get_single_booking_by_user_not_found(self):
		try:
			response = self.app.get(
				'/api/users/wrong_user/bookings/wrong_booking',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)
	
	def test_get_single_booking_by_user_forbidden(self):
		try:
			response = self.app.get(
				'/api/users/user1_uuid/bookings/b3_uuid',
				headers={'x-access-token': self.user_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, ForbiddenError)
	
	def test_get_single_booking_by_user_forbidden(self):
		try:
			response = self.app.get(
				'/api/users/user3_uuid/bookings/b3_uuid',
				headers={'x-access-token': self.user_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, ForbiddenError)
	
	def test_cancel_booking_as_admin(self):
		response = self.app.put(
			'/api/users/user1_uuid/bookings/b1_uuid/cancel',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)
	
	def test_cancel_booking_as_user(self):
		response = self.app.put(
			'/api/users/user1_uuid/bookings/b1_uuid/cancel',
			headers={'x-access-token': self.user_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)

	def test_cancel_booking_forbidden(self):
		try:
			response = self.app.put(
				'/api/users/user2_uuid/bookings/b3_uuid/cancel',
				headers={'x-access-token': self.user_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, ForbiddenError)

	def test_cancel_booking_not_found(self):
		try:
			response = self.app.put(
				'/api/users/user1_uuid/bookings/wrong_booking/cancel',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)

	def test_cancel_user_not_found(self):
		try:
			response = self.app.put(
				'/api/users/wrong_user/bookings/b1_uuid/cancel',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)



if __name__ == "__main__":
	unittest.main()