""" Testing station endpoints """

__author__ = "Markus Mogck"
__version__ = "1.0"

import os
import unittest
import base64
import json
 
from flask import current_app as app
from application import db

from application.resources.errors import *


class StationsTest(unittest.TestCase):

	###############################################################################################

	# executed prior to each test
	def setUp(self):
		# Set up the app
		from application import create_app
		from config import TestConfig
		app = create_app(config=TestConfig)

		# Clear and re-initialize the test database
		from application.database.db_init_values_test import initialize_test_values
		from application.database.db_init_values import initialize_admin, initialize_values
		with app.app_context():
			db.drop_all()
			db.create_all()
			initialize_values()
			initialize_admin()
			initialize_test_values()

		# Initialize simulated Client
		self.app = app.test_client()
 
		self.assertEqual(app.debug, False)

		# Set jwt tokens for later use / tests
		self.admin_token = self.get_token('admin', 'admin')
		self.employee_token = self.get_token('employee1', 'employee1')
		self.user_token = self.get_token('user1', 'user1')

	# executed after each test
	def tearDown(self):
		pass

	# Helper method to login
	def login(self, username, password):
		return self.app.get(
			'/api/login',
			headers={'Authorization': 'Basic ' + base64.b64encode(bytes(username + ":" + password, 'ascii')).decode('ascii')},
			follow_redirects=True
		)
	
	# Helper method to login that returns only the jwt token
	def get_token(self, username, password):
		import json
		response = self.login(username, password)
		self.assertIn(b'token', response.data)
		data = json.loads(response.get_data(as_text=True))
		return data['token']
		
	###############################################################################################

	def test_get_all_stations_authorized(self):
		response = self.app.get(
			'/api/stations',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)

	def test_post_station(self):
		response = self.app.post(
			'/api/stations',
			headers={'x-access-token': self.admin_token},
			data = json.dumps(
				dict(
					name='Test Station',
					address='Test Address',
					post_code='034692',
					city='Alsleben',
					country='Deutschland'
				)
			),
			content_type='application/json',
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 201)

	def test_post_station_unauthorized(self):
		try:
			response = self.app.post(
				'/api/stations',
				headers={'x-access-token': self.user_token},
				data = json.dumps(
					dict(
						name='Test Station',
						address='Test Address',
						post_code='034692',
						city='Alsleben',
						country='Deutschland'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, ForbiddenError)

	def test_post_station_no_data(self):
		try:
			response = self.app.post(
				'/api/stations',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						# name is missing
						address='Test Address',
						post_code='034692',
						city='Alsleben',
						country='Deutschland'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_post_station_wrong_data1(self):
		try:
			response = self.app.post(
				'/api/stations',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						name='',
						address='Test Address',
						post_code='034692',
						city='Alsleben',
						country='Deutschland'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_post_station_wrong_data2(self):
		try:
			response = self.app.post(
				'/api/stations',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						name='Test Name',
						address=True,
						post_code='034692',
						city='Alsleben',
						country='Deutschland'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_post_station_already_exists(self):
		try:
			response = self.app.post(
				'/api/stations',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						name='S1',
						address=True,
						post_code='034692',
						city='Alsleben',
						country='Deutschland'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_delete_all_stations_authorized(self):
		response_delete_bookings = self.app.delete(
			'/api/bookings',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		response_delete_vehicles =self.app.delete(
			'/api/vehicles',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		response_delete_stations = self.app.delete(
			'/api/stations',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response_delete_stations.status_code, 204)

	def test_delete_all_stations_forbidden(self):
		try:
			response = self.app.delete(
				'/api/stations',
				headers={'x-access-token': self.user_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, ForbiddenError)

	def test_delete_all_stations_integrity_error(self):
		try:
			response = self.app.delete(
				'/api/stations',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, InternalServerError)

	def test_get_single_station(self):
		response = self.app.get(
			'/api/stations/s1_uuid',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)
	
	def test_get_single_station_not_found(self):
		try:
			response = self.app.get(
				'/api/stations/nonexistent_station',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)

	def test_put_station_basic_updates(self):
		response = self.app.put(
			'/api/stations/s1_uuid',
			headers={'x-access-token': self.admin_token},
			data = json.dumps(
				dict(
					address='new address',
					post_code='133337',
					city='new city',
					country='new country'
				)
			),
			content_type='application/json',
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)

	def test_put_station_update_name(self):
		response = self.app.put(
			'/api/stations/s1_uuid',
			headers={'x-access-token': self.admin_token},
			data = json.dumps(
				dict(
					name='new name',
					address='new address',
					post_code='133337',
					city='new city',
					country='new country'
				)
			),
			content_type='application/json',
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)

	def test_put_station_not_found(self):
		try:
			response = self.app.put(
				'/api/stations/nonexistent_station',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						name='new name',
						address='new address',
						post_code='133337',
						city='new city',
						country='new country'
					)
				),
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)

	def test_put_station_name_already_exits(self):
		try:
			response = self.app.put(
				'/api/stations/s1_uuid',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						name='S2',
						address='new address',
						post_code='133337',
						city='new city',
						country='new country'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, ConflictError)

	def test_delete_station(self):
		response = self.app.delete(
			'/api/stations/s4_uuid',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 204)

	def test_delete_station_not_found(self):
		try:
			response = self.app.delete(
				'/api/stations/nonexistent_station',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)

	def test_get_vehicles_by_station(self):
		response = self.app.get(
			'/api/stations/s1_uuid/vehicles',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)
	
	def test_get_vehicles_by_station_not_found(self):
		try:
			response = self.app.get(
				'/api/stations/nonexistent_station/vehicles',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)
			

if __name__ == "__main__":
	unittest.main()