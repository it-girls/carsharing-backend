""" Testing user endpoints """

__author__ = "Markus Mogck"
__version__ = "1.0"

import os
import unittest
import base64
import json
 
from flask import current_app as app
from application import db

from application.resources.errors import *


class UserTest(unittest.TestCase):

	###############################################################################################

	# executed prior to each test
	def setUp(self):
		# Set up the app
		from application import create_app
		from config import TestConfig
		app = create_app(config=TestConfig)

		# Clear and re-initialize the test database
		from application.database.db_init_values_test import initialize_test_values
		from application.database.db_init_values import initialize_admin, initialize_values
		with app.app_context():
			db.drop_all()
			db.create_all()
			initialize_values()
			initialize_admin()
			initialize_test_values()

		# Initialize simulated Client
		self.app = app.test_client()
 
		self.assertEqual(app.debug, False)

		# Set jwt tokens for later use / tests
		self.admin_token = self.get_token('admin', 'admin')
		self.employee_token = self.get_token('employee1', 'employee1')
		self.user_token = self.get_token('user1', 'user1')

	# executed after each test
	def tearDown(self):
		pass

	# Helper method to login
	def login(self, username, password):
		return self.app.get(
			'/api/login',
			headers={'Authorization': 'Basic ' + base64.b64encode(bytes(username + ":" + password, 'ascii')).decode('ascii')},
			follow_redirects=True
		)
	
	# Helper method to login that returns only the jwt token
	def get_token(self, username, password):
		import json
		response = self.login(username, password)
		self.assertIn(b'token', response.data)
		data = json.loads(response.get_data(as_text=True))
		return data['token']
		
	###############################################################################################

	# Most database changes do not need to be checked directly,
	# since the status code is only sent without error messages after a successful change

	def test_get_all_users_authorized(self):
		response = self.app.get(
			'/api/users',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)
	
	def test_get_all_users_unauthorized(self):
		try:
			response = self.app.get(
				'/api/users',
				headers={'x-access-token': self.user_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, ForbiddenError)

	def test_post_user(self):
		response = self.app.post(
			'/api/users',
			headers={'x-access-token': self.user_token},
			data = json.dumps(
				dict(
					email='new@user.de',
					password='123',
					first_name='Bonstantin',
					last_name='Cerg',
					birthdate='2004-06-14T23:34:30.0000Z',
					id_number='ABC123',
					license_number='DEF456'
				)
			),
			content_type='application/json',
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 201)

	def test_post_user_already_existing(self):
		try:
			response = self.app.post(
				'/api/users',
				headers={'x-access-token': self.user_token},
				data = json.dumps(
					dict(
						email='user1',
						password='user1',
						first_name='User',
						last_name='One',
						birthdate='1970-06-12T00:00:00.0000Z',
						id_number='T22000129',
						license_number='B072RRE2I55'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_post_user_no_data(self):
		try:
			response = self.app.post(
				'/api/users',
				headers={'x-access-token': self.user_token},
				data = json.dumps(
					dict(
						email='',
						password='',
						first_name='Bonstantin',
						last_name='Cerg',
						birthdate='2004-06-14T23:34:30.0000Z',
						id_number='ABC123',
						license_number='DEF456'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_post_user_wrong_data(self):
		try:
			response = self.app.post(
				'/api/users',
				headers={'x-access-token': self.user_token},
				data = json.dumps(
					dict(
						email='new@user.de',
						password='123',
						first_name='Bonstantin',
						last_name='Cerg',
						birthdate='jan stinkt',
						id_number='ABC123',
						license_number='DEF456'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_delete_all(self):
		response = self.app.delete(
			'/api/bookings',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		response = self.app.delete(
			'/api/users',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 204)

	def test_delete_all_integrity_error(self):
		try:
			response = self.app.delete(
				'/api/users',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, InternalServerError)

	def test_delete_all_unauthorized(self):
		try:
			response = self.app.delete(
				'/api/users',
				headers={'x-access-token': self.user_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, ForbiddenError)

	def test_get_single_user_with_power(self):
		response = self.app.get(
			'/api/users/user1_uuid',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)

	def test_get_single_user_with_auth(self):
		response = self.app.get(
			'/api/users/user1_uuid',
			headers={'x-access-token': self.user_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)

	def test_get_single_user_not_found(self):
		try:
			response = self.app.get(
				'/api/users/nonexistent_uuid',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)

	def test_put_user_not_found(self):
		try:
			response = self.app.put(
				'/api/users/nonexistent_uuid',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						email='user1',
						password='user1',
						first_name='User',
						last_name='One',
						birthdate='1970-06-12T00:00:00.0000Z',
						id_number='T22000129',
						license_number='B072RRE2I55'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)

	def test_put_user_update_email(self):
		response = self.app.put(
			'/api/users/user1_uuid',
			headers={'x-access-token': self.user_token},
			data = json.dumps(
				dict(
					email='new_email'
				)
			),
			content_type='application/json',
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)

	def test_put_user_update_email_already_used(self):
		try:
			response = self.app.put(
				'/api/users/user1_uuid',
				headers={'x-access-token': self.user_token},
				data = json.dumps(
					dict(
						email='user2'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, ConflictError)

	def test_put_user_update_password(self):
		response = self.app.put(
			'/api/users/user1_uuid',
			headers={'x-access-token': self.user_token},
			data = json.dumps(
				dict(
					password='new_password'
				)
			),
			content_type='application/json',
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)

	def test_put_user_update_role(self):
		response = self.app.put(
			'/api/users/user1_uuid',
			headers={'x-access-token': self.admin_token},
			data = json.dumps(
				dict(
					user_role='mitarbeiter'
				)
			),
			content_type='application/json',
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)

	def test_put_user_update_role_bad_request(self):
		try:
			response = self.app.put(
				'/api/users/user1_uuid',
				headers={'x-access-token': self.user_token},
				data = json.dumps(
					dict(
						user_role='does_not_exist'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_put_user_update_role_unauthorized_as_user(self):
		try:
			response = self.app.put(
				'/api/users/user1_uuid',
				headers={'x-access-token': self.user_token},
				data = json.dumps(
					dict(
						user_role='admin'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, UnauthorizedError)

	def test_put_user_update_role_unauthorized_as_employee(self):
		try:
			response = self.app.put(
				'/api/users/user1_uuid',
				headers={'x-access-token': self.employee_token},
				data = json.dumps(
					dict(
						user_role='admin'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, UnauthorizedError)

	def test_put_user_update_noncritical_values(self):
		response = self.app.put(
			'/api/users/user1_uuid',
			headers={'x-access-token': self.admin_token},
			data = json.dumps(
				dict(
					first_name='new_first_name',
					last_name='new_last_name',
					id_number='new_id_number',
					license_number='new_license_number'
				)
			),
			content_type='application/json',
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)

	def test_delete_single_user(self):
		response = self.app.delete(
			'/api/users/employee2_uuid',
			headers={'x-access-token': self.admin_token},
			content_type='application/json',
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 204)

	def test_delete_single_user_not_found(self):
		try:
			response = self.app.delete(
				'/api/users/nonexistent_uuid',
				headers={'x-access-token': self.admin_token},
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)


if __name__ == "__main__":
	unittest.main()