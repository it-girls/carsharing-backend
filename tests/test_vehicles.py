""" Testing vehicle endpoints """

__author__ = "Markus Mogck"
__version__ = "1.0"

import os
import unittest
import base64
import json
 
from flask import current_app as app
from application import db

from application.resources.errors import *


class VehiclesTest(unittest.TestCase):

	###############################################################################################

	# executed prior to each test
	def setUp(self):
		# Set up the app
		from application import create_app
		from config import TestConfig
		app = create_app(config=TestConfig)

		# Clear and re-initialize the test database
		from application.database.db_init_values_test import initialize_test_values
		from application.database.db_init_values import initialize_admin, initialize_values
		with app.app_context():
			db.drop_all()
			db.create_all()
			initialize_values()
			initialize_admin()
			initialize_test_values()

		# Initialize simulated Client
		self.app = app.test_client()
 
		self.assertEqual(app.debug, False)

		# Set jwt tokens for later use / tests
		self.admin_token = self.get_token('admin', 'admin')
		self.employee_token = self.get_token('employee1', 'employee1')
		self.user_token = self.get_token('user1', 'user1')

	# executed after each test
	def tearDown(self):
		pass

	# Helper method to login
	def login(self, username, password):
		return self.app.get(
			'/api/login',
			headers={'Authorization': 'Basic ' + base64.b64encode(bytes(username + ":" + password, 'ascii')).decode('ascii')},
			follow_redirects=True
		)
	
	# Helper method to login that returns only the jwt token
	def get_token(self, username, password):
		import json
		response = self.login(username, password)
		self.assertIn(b'token', response.data)
		data = json.loads(response.get_data(as_text=True))
		return data['token']
		
	###############################################################################################

	def test_get_all_vehicles_authorized(self):
		response = self.app.get(
			'/api/vehicles',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)
	
	def test_get_all_vehicles_forbidden(self):
		try:
			response = self.app.get(
				'/api/vehicles',
				headers={'x-access-token': self.user_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, ForbiddenError)

	def test_post_vehicle(self):
		response = self.app.post(
			'/api/vehicles',
			headers={'x-access-token': self.admin_token},
			data = json.dumps(
				dict(
					chassis_number='new_c_number',
					license_plate='new_lp',
					public_station_id='s1_uuid',
					comment='new comment',
					brand='new brand',
					model='new model',
					category='J',
					vehicle_type='T',
					transmission='N',
					addition='H'
				)
			),
			content_type='application/json',
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 201)

	def test_post_vehicle_unauthorized(self):
		try:
			response = self.app.post(
				'/api/vehicles',
				headers={'x-access-token': self.user_token},
				data = json.dumps(
					dict(
						chassis_number='new_c_number',
						license_plate='new_lp',
						public_station_id='s1_uuid',
						comment='new comment',
						brand='new brand',
						model='new model',
						category='J',
						vehicle_type='T',
						transmission='N',
						addition='H'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, ForbiddenError)

	def test_post_vehicle_no_data(self):
		try:
			response = self.app.post(
				'/api/vehicles',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						# no chassis number
						license_plate='new_lp',
						public_station_id='s1_uuid',
						comment='new comment',
						brand='new brand',
						model='new model',
						category='J',
						vehicle_type='T',
						transmission='N',
						addition='H'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_post_vehicle_wrong_data1(self):
		try:
			response = self.app.post(
				'/api/vehicles',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						chassis_number='new_c_number',
						license_plate='new_lp',
						public_station_id='',
						comment='new comment',
						brand='new brand',
						model='new model',
						category='J',
						vehicle_type='T',
						transmission='N',
						addition='H'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_post_vehicle_wrong_data2(self):
		try:
			response = self.app.post(
				'/api/vehicles',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						chassis_number='new_c_number',
						license_plate='new_lp',
						public_station_id='wrong station id',
						comment='new comment',
						brand='new brand',
						model='new model',
						category='J',
						vehicle_type='T',
						transmission='N',
						addition='H'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_post_vehicle_wrong_data3(self):
		try:
			response = self.app.post(
				'/api/vehicles',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						chassis_number='new_c_number',
						license_plate='new_lp',
						public_station_id='s1_uuid',
						comment='new comment',
						brand='new brand',
						model='new model',
						category='J',
						vehicle_type='T',
						transmission='N',
						addition='not existing'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_post_vehicle_wrong_data4(self):
		try:
			response = self.app.post(
				'/api/vehicles',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						chassis_number='',
						license_plate='new_lp',
						public_station_id='s1_uuid',
						comment='new comment',
						brand='new brand',
						model='new model',
						category='J',
						vehicle_type='T',
						transmission='N',
						addition='H'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_post_vehicle_chassis_number_already_exists(self):
		try:
			response = self.app.post(
				'/api/vehicles',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						chassis_number='C1',
						license_plate='new_lp',
						public_station_id='s1_uuid',
						comment='new comment',
						brand='new brand',
						model='new model',
						category='J',
						vehicle_type='T',
						transmission='N',
						addition='H'
					)
				),
				content_type='application/json',
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, BadRequestError)

	def test_delete_all_vehicles_authorized(self):
		response_delete_bookings = self.app.delete(
			'/api/bookings',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		response_delete_vehicles = self.app.delete(
			'/api/vehicles',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response_delete_vehicles.status_code, 204)

	def test_delete_all_vehicles_integrity_error(self):
		try:
			response = self.app.delete(
				'/api/vehicles',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, InternalServerError)

	def test_delete_all_vehicles_forbidden(self):
		try:
			response = self.app.delete(
				'/api/vehicles',
				headers={'x-access-token': self.user_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, ForbiddenError)

	def test_get_single_vehicle(self):
		response = self.app.get(
			'/api/vehicles/v1_uuid',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)
	
	def test_get_single_vehicle_not_found(self):
		try:
			response = self.app.get(
				'/api/vehicles/nonexistent_vehicle',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)

	def test_put_vehicle(self):
		response = self.app.put(
			'/api/vehicles/v1_uuid',
			headers={'x-access-token': self.admin_token},
			data = json.dumps(
				dict(
					license_plate='new_lp',
					public_station_id='s1_uuid',
					comment='new comment',
					brand='new brand',
					model='new model',
					category='J',
					vehicle_type='T',
					transmission='N',
					addition='H'
				)
			),
			content_type='application/json',
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 200)

	def test_put_vehicle_not_found(self):
		try:
			response = self.app.put(
				'/api/vehicles/nonexistent_vehicle',
				headers={'x-access-token': self.admin_token},
				data = json.dumps(
					dict(
						license_plate='new_lp',
						public_station_id='s1_uuid',
						comment='new comment',
						brand='new brand',
						model='new model',
						category='J',
						vehicle_type='T',
						transmission='N',
						addition='H'
					)
				),
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)

	def test_delete_vehicle(self):
		response = self.app.delete(
			'/api/vehicles/v7_uuid',
			headers={'x-access-token': self.admin_token},
			follow_redirects=True
		)
		self.assertEqual(response.status_code, 204)

	def test_delete_vehicle_not_found(self):
		try:
			response = self.app.delete(
				'/api/vehicles/nonexistent_vehicle',
				headers={'x-access-token': self.admin_token},
				follow_redirects=True
			)
		except Exception as exception:
			self.assertIsInstance(exception, NotFoundError)


if __name__ == "__main__":
	unittest.main()