""" Starting point for the web service gateway interface """

__author__ = "Markus Mogck"
__version__ = "1.0"

import sys
from application import create_app
from config import DevConfig, ProdConfig, TestConfig


config_run = None

if len(sys.argv) > 1:
	config_in = str(sys.argv[1])
	if config_in == 'DevConfig':
		config_run = DevConfig
		print('Running with Development Config')
	elif config_in == 'ProdConfig':
		config_run = ProdConfig
		print('Running with Production Config')
	elif config_in == 'TestConfig':
		config_run = TestConfig
		print('Running with Test Config')
	else:
		config_run = ProdConfig
		print('Running with Production Config')
else:
	config_run = ProdConfig
	print('Running with Production Config')

app = create_app(config=config_run)

if __name__ == "__main__":
	from config import APP_HOST
	from config import APP_PORT
	from config import APP_SSL

	app.run(host=APP_HOST)
	